const request = fetch("https://ajax.test-danit.com/api/swapi/films");
const ulFilms = document.querySelector("#filmsUl");

request
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
    data.forEach((element) => {
      const filmLi = document.createElement("li");
      filmLi.textContent = element.name + " " + element.episodeId;
      ulFilms.append(filmLi);
      const characters = element.characters;
      for (key of characters) {
        const characterUrl = fetch(key);
        characterUrl
          .then((response) => response.json())
          .then((data) => {
            const charactersLi = document.createElement("p");
            charactersLi.textContent = data.name;
            filmLi.children[0].before(charactersLi)
          });
      }
      const filmDescription = document.createElement("p");
      filmDescription.textContent = element.openingCrawl;
      filmLi.append(filmDescription);
    });
  });
