// 1 - процес, що дозволяє одному об'єкту базуватися на іншому і розділяти спільні властивості.
// 2 - використовується для виклику батьківського конструктора і можливості наслідувати його ключі

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  set age(value) {
    if (value > 0) {
      this._age = value;
    }
  }
  get age() {
    return `У користувача ${this._name} вік ${this._age}`;
  }
  set name(newName) {
    if (newName.length > 2) {
      this._name = newName;
    }
  }
  get name() {
    return `У користувача ім'я ${this._name}`;
  }

  set salary(newSalary) {
    if (newSalary > 0) {
      this._salary = newSalary;
    }
  }
  get salary() {
    return `У користувача зарплата ${this._salary}`;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary)
    this._lang = lang
  }
  get salary() {
    return this._salary*3
  }
}

const prog1 = new Programmer("Вася", 27, 3500, "English")
const prog2 = new Programmer("Діма", 34, 1400, "German")
const prog3 = new Programmer("Віка", 23, 4200, "Polish")
console.log(prog1,prog2,prog3)
console.log(prog2.salary)
