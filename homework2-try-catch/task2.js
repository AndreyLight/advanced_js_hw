// 1 - загалом try...catch можна використовувати як фільтр для будь якого набору даних,
// де некоректне значення буде відсортовуватись через catch

const ul = document.querySelector("ul");
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

books.forEach((book, index) => {
  try {
    if (!book.author) {
      throw new Error(`В книзі ${index + 1} немає автора`);
    }
    if (!book.name) {
      throw new Error(`В книзі ${index + 1} немає назви`);
    }
    if (!book.price) {
      throw new Error(`В книзі ${index + 1} немає ціни`);
    }
    const li = document.createElement("li")
    li.innerHTML = `
    <p>Автор книги: ${book.author}</p>
    <p>Назва книги: ${book.name}</p>
    <p>Ціна книги: ${book.price}</p>
    `
    ul.append(li)
  } catch (error) {
    console.log(error);
  }
});

